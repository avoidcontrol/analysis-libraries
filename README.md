## Depricated: moved to [Ticketlos Platform Repo](https://gitlab.com/ticketlos_berlin/platform/-/tree/master/)

# Ticketlos Data Analysis Libraries

## Packages

- @ticketlos/geospatial - Functions for geospatial analysis, which mostly return geojson.
- @ticketlos/nlp - Simple NLP functions.

## Contributing

CI runs linting and tests on every push.

Make a tag to run deployment build proccesses to publish to the NPM package repo. Remember to increment the version.

Idea: point pattern analysis to create clusters. Then return the stations in the cluster.
