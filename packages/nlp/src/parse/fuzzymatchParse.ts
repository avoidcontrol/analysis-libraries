import fuzzysort from 'fuzzysort';

import { createTrigrams } from '../utils';

interface FuzzySearchResult {
  key: string;
  score: number;
  target: string;
}

/**
 * Returns the value from a hash map where the key is a substring
 * to match in another string.
 *
 * Options allow for changing the number of results (default 1)
 * or the match threshold for the key in each trigram (default -6000)
 *
 * @param input
 * @param searchString
 * @param options
 */
const fuzzymatchParse = (
  input: { [key: string]: any },
  searchString: string,
  options?: { numResults?: number; matchThreshold?: number },
): any | any[] | undefined => {
  // Get options from args or set default options
  const { numResults = 1, matchThreshold = -6000 } = options || {};
  // If requested number of results is not 1 or greater
  if (numResults <= 0)
    throw new Error('Option numResults must be greater than 1');

  // Create trigrams from text string
  const textTrigrams = createTrigrams(searchString);

  // Loop thru each key in hash set
  const searchResults: FuzzySearchResult[] = Object.keys(input)
    .reduce((acc: FuzzySearchResult[], key: string) => {
      // Loop through each trigram in text
      const trigramMatches = textTrigrams.reduce(
        (acc: FuzzySearchResult[], trigramList: string[]) => {
          const joinedTrigram = trigramList.join(' ');
          // Check fuzzy match of hash set key in joined trigram
          const result = fuzzysort.single(key, joinedTrigram);
          // If match, add to list of matches
          if (result && result.score >= matchThreshold) {
            return [
              ...acc,
              {
                key: key,
                score: result.score,
                target: result.target,
              },
            ];
          }
          return acc;
        },
        [],
      );
      // Unpack all matches in to results accumulator
      return [...acc, ...trigramMatches];
    }, [])
    // Sort matches by score
    .sort((first: FuzzySearchResult, second: FuzzySearchResult) => {
      if (first.score > second.score) return -1;
      if (second.score > first.score) return 1;
      else return 0;
    });

  // If no matches
  if (searchResults.length === 0) {
    return undefined;
  }
  // If top result requested
  if (numResults === 1) {
    // Return best result
    return input[searchResults[0].key];
  }
  // If multiple results requested return list of best results
  return [...Array(numResults).keys()].reduce((acc: any[], index: number) => {
    // If there are search results at the index
    if (index <= searchResults.length - 1) {
      return [...acc, input[searchResults[index].key]];
    }
    return acc;
  }, []);
};

export { fuzzymatchParse };
