import { fuzzymatchParse } from './fuzzymatchParse';

describe('fuzzymatchParse.ts', () => {
  describe('fuzzymatchParse', () => {
    describe('returns value from hashmap where key is string matched in text', () => {
      it('with one input', () => {
        const input = {
          'konga line': {
            name: 'konga line',
            stupid: 'yes',
          },
        };
        const result = fuzzymatchParse(
          input,
          'my favourite dance is the konga line',
        );
        expect(result).toEqual(input['konga line']);
      });

      it('with multiple inputs', () => {
        const input = {
          'tom delong': {
            name: 'Tom Delong',
            stupid: 'yes',
          },
          'Mark Hoppus': {
            name: 'Mark Hoppus',
            stupid: 'yes',
          },
          'Travis Barker': {
            name: 'Travis Barker',
            stupid: 'yes',
          },
        };
        const result = fuzzymatchParse(
          input,
          'The bass guitar player of Blink-182 was Mark Hoppus',
        );
        expect(result).toEqual(input['Mark Hoppus']);
      });
    });

    describe('can return multiple results', () => {
      it('when the top three results are requested', () => {
        const input = {
          'Hallesches Tor': {
            name: 'Hallesches Tor',
          },
          Hermannstrasse: {
            name: 'Hermannstrasse',
          },
          Hermanstrasse: {
            name: 'Hermannstrasse',
          },
        };
        const result = fuzzymatchParse(
          input,
          'Incident at Hermannstrasse station today',
          { numResults: 2 },
        );
        expect(result.length).toEqual(2);
      });

      it('returns as many results as is possible if numResults longer than number of matches', () => {
        const input = {
          'Hallesches Tor': {
            name: 'Hallesches Tor',
          },
          Hermannstrasse: {
            name: 'Hermannstrasse',
          },
          Hermanstrasse: {
            name: 'Hermannstrasse',
          },
        };
        const result = fuzzymatchParse(
          input,
          'Incident at Hermannstrasse station today',
          { numResults: 5 },
        );
        // TODO: get this to 2
        expect(result.length).toEqual(3);
      });
    });
  });
});
