import { hashmap } from './hashmap';

describe('hashmap', () => {
  describe('returns a hash map', () => {
    it('with top level property', () => {
      const input = [
        {
          name: 'Jerry',
        },
        {
          name: 'George',
        },
        {
          name: 'Elaine',
        },
        {
          name: 'Cosmo',
        },
      ];
      const result = hashmap(input, 'name');
      expect(Object.keys(result).length === 4).toEqual(true);
    });

    it('with nested property', () => {
      const input = [
        {
          city: {
            name: 'San Francisco',
          },
        },
        {
          city: {
            name: 'Berlin',
          },
        },
        {
          city: {
            name: 'London',
          },
        },
        {
          city: {
            name: 'Tokyo',
          },
        },
      ];
      const result = hashmap(input, ['city', 'name']);
      expect(Object.keys(result).length === 4).toEqual(true);
    });

    it('with array at top level property', () => {
      const input = [
        {
          famousPeople: ['Harvey Milk', 'Vince Lombardi'],
        },
        {
          famousPeople: ['Some German guy', 'Johannes'],
        },
        {
          famousPeople: ['Romesh Ranganathan'],
        },
        {
          famousPeople: ['Kojima'],
        },
      ];

      const result = hashmap(input, 'famousPeople');
      expect(Object.keys(result).length === 6).toEqual(true);
    });

    it('with array at nested property', () => {
      const input = [
        {
          city: {
            name: 'San Francisco',
            famousPeople: ['Harvey Milk', 'Vince Lombardi'],
          },
        },
        {
          city: {
            name: 'Berlin',
            famousPeople: ['Some German guy'],
          },
        },
        {
          city: {
            name: 'London',
            famousPeople: ['Romesh Ranganathan', 'Stormzy'],
          },
        },
        {
          city: {
            name: 'Tokyo',
            famousPeople: ['Kojima'],
          },
        },
      ];
      const result = hashmap(input, ['city', 'famousPeople']);
      expect(Object.keys(result).length === 6).toEqual(true);
    });

    it('with one top level additional property', () => {
      const input = [
        {
          name: 'Jerry',
          nickname: 'Jer',
        },
        {
          name: 'George',
          nickname: 'GeorgieBoy',
        },
        {
          name: 'Elaine',
          nickname: 'Laney',
        },
        {
          name: 'Cosmo',
          nickname: 'Kramer',
        },
      ];
      const result = hashmap(input, 'name', ['nickname']);
      expect(Object.keys(result).length === 8).toEqual(true);
    });

    it('with one nested additional property', () => {
      const input = [
        {
          city: {
            name: 'San Francisco',
            famousPeople: 'Harvey Milk',
          },
        },
        {
          city: {
            name: 'Berlin',
            famousPeople: 'Some German guy',
          },
        },
        {
          city: {
            name: 'London',
            famousPeople: 'Romesh Ranganathan',
          },
        },
        {
          city: {
            name: 'Tokyo',
            famousPeople: 'Kojima',
          },
        },
      ];
      const result = hashmap(
        input,
        ['city', 'name'],
        [['city', 'famousPeople']],
      );
      expect(Object.keys(result).length === 8).toEqual(true);
    });

    it('with array at nested additional property', () => {
      const input = [
        {
          city: {
            name: 'San Francisco',
            famousPeople: ['Harvey Milk', 'Easy E'],
          },
        },
        {
          city: {
            name: 'Berlin',
            famousPeople: 'Some German guy',
          },
        },
        {
          city: {
            name: 'London',
            famousPeople: ['Romesh Ranganathan', 'Wiley'],
          },
        },
        {
          city: {
            name: 'Tokyo',
            famousPeople: 'Kojima',
          },
        },
      ];
      const result = hashmap(
        input,
        ['city', 'name'],
        [['city', 'famousPeople']],
      );
      expect(Object.keys(result).length === 10).toEqual(true);
    });

    it('with multiple top level additional properties', () => {
      const input = [
        {
          name: 'San Francisco',
          famousPeople: ['Harvey Milk', 'Easy E'],
          country: 'USA',
        },
        {
          name: 'Berlin',
          famousPeople: 'Some German guy',
          country: 'DE',
        },
        {
          name: 'London',
          famousPeople: ['Romesh Ranganathan', 'Wiley'],
          country: 'UK',
        },
        {
          name: 'Tokyo',
          famousPeople: 'Kojima',
          country: 'JP',
        },
      ];
      const result = hashmap(input, 'name', ['famousPeople', 'country']);
      expect(Object.keys(result).length === 14).toEqual(true);
    });

    it('with multiple nested additional properties', () => {
      const input = [
        {
          prop1: {
            innerProp1: 'Test 1',
          },
          prop2: {
            innerProp2: 'Test 10',
          },
          name: 'Name 1',
        },
        {
          prop1: {
            innerProp1: 'Test 2',
          },
          prop2: {
            innerProp2: 'Test 20',
          },
          name: 'Name 2',
        },
        {
          prop1: {
            innerProp1: 'Test 3',
          },
          prop2: {
            innerProp2: 'Test 30',
          },
          name: 'Name 3',
        },
      ];
      const result = hashmap(
        input,
        ['prop1', 'innerProp1'],
        ['name', ['prop2', 'innerProp2']],
      );
      expect(Object.keys(result).length === 9).toEqual(true);
    });
  });
});
