const getNestedValue = (item: {}, prop: string[]): any =>
  // Loop through each level of nested objects
  prop.reduce((acc: {}, propName: string) => {
    // If property to search for exists on this level
    if (acc.hasOwnProperty(propName)) {
      // Return value at the key to search for, ending on final value
      return acc[propName];
    }
    return acc;
    // Starts with object
  }, item);

/**
 * Returns a hashmap where the key is the selected prop value, and the value
 * is the top level object that is looped through. When additional props
 * are passed, new entries are made in hashmap which point to
 * the same object.
 *
 * The prop values that are used as keys should be unique. If the prop
 * points to a list, items will be made for each value. If object,
 * null, or boolean, will return error
 *
 *
 * @param input
 * @param prop
 * @param additionalProps
 */
export const hashmap = (
  input: {}[],
  prop: string | string[],
  additionalProps?: Array<string | string[]>,
): { [key: string]: any } => {
  const propNested = Array.isArray(prop) && prop.length > 0;
  const additionalPropsExist =
    additionalProps != null &&
    Array.isArray(additionalProps) &&
    additionalProps.length > 0;

  return input.reduce((acc: {}, item: {}) => {
    // Get prop value
    const propValue: string | string[] = propNested
      ? getNestedValue(item, prop as string[])
      : item[prop as string];

    const propValueIsArray = Array.isArray(propValue) && propValue.length > 0;
    const propValueStringArray = propValueIsArray
      ? // Already an array
        (propValue as string[])
      : // Put single value in to array
        [propValue as string];
    // Build dict to add to hashmap for item
    const selectedPropDict = propValueStringArray.reduce((acc, value) => {
      return {
        ...acc,
        [value]: item,
      };
    }, {});

    // Get all values from additional props
    const additionalPropsValues = additionalPropsExist
      ? (additionalProps as any[]).map((additionalProp) => {
          // If is nested
          const isNested =
            Array.isArray(additionalProp) && additionalProp.length > 0;
          // Return value from nested obj
          if (isNested) return getNestedValue(item, additionalProp);
          // Return value from top level
          return item[additionalProp];
        })
      : [];
    const flatAdditionalPropsValues = additionalPropsValues.reduce(
      (acc, value) => {
        return acc.concat(value, []);
      },
      [],
    );
    const additionalPropsDict = flatAdditionalPropsValues.reduce(
      (acc: {}, value: any) => {
        return {
          ...acc,
          [value]: item,
        };
      },
      {},
    );

    return {
      ...acc,
      ...selectedPropDict,
      ...additionalPropsDict,
    };
  }, {});
};
