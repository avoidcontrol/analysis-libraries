import { createTrigrams } from './utils';

describe('utils.ts', () => {
  describe('createTrigrams', () => {
    it('returns trigrams from sentence', () => {
      const exampleText = 'The sea was angry that day my friends';
      const result = createTrigrams(exampleText);
      expect(result.length).toEqual(6);
      expect(result.every((item) => item.length === 3)).toBe(true);
    });

    it('returns correct result from sentence less than three words', () => {
      const exampleText = "I'm gay";
      const result = createTrigrams(exampleText);
      expect(result.length).toEqual(1);
      expect(result.every((item) => item.length === 2)).toBe(true);
    });
  });
});
