import {
  featureCollection as createFeatureCollection,
  Feature,
  FeatureCollection,
} from '@turf/helpers';
import booleanEqual from '@turf/boolean-equal';

/**
 * Returns a feature collection that contains features with unique locations,
 * and puts a count of how many features in original data set were at this location
 *
 * Optionally can collect the specified props across the features in to a list.
 *
 * @param featureCollection
 * @param propsToCopy
 */
const countPointsAtLocation = (
  featureCollection: FeatureCollection,
  propsToCopy?: string[],
): FeatureCollection => {
  const countedFeatures: Feature[] = featureCollection.features.reduce(
    (acc: Feature[], feature: Feature) => {
      // Look for feature in acc
      const featureIndex = acc.findIndex((compareFeature) => {
        const isEqual = booleanEqual(feature.geometry, compareFeature.geometry);
        return isEqual;
      });
      const alreadyCounted = featureIndex >= 0;

      // If feature does not yet exist in acc
      if (!alreadyCounted) {
        // Create list from props
        const listifiedProps =
          propsToCopy != null
            ? propsToCopy.reduce((acc, prop) => {
                const copiedProp = feature.properties[prop]
                  ? { [prop]: [feature.properties[prop]] }
                  : {};
                return {
                  ...acc,
                  ...copiedProp,
                };
              }, {})
            : {};

        // Add to acc
        return [
          ...acc,
          {
            ...feature,
            properties: {
              ...feature.properties,
              ...listifiedProps,
              count: 1,
            },
          },
        ];
      }

      // If feature already exists
      if (alreadyCounted) {
        const foundFeature = acc[featureIndex];
        const count = foundFeature.properties.count;

        // Get create lists for props
        const listifiedProps =
          propsToCopy != null
            ? propsToCopy.reduce((acc, prop) => {
                // Copy prop value to list, or return empty obj
                const copiedProp = foundFeature.properties[prop]
                  ? {
                      [prop]: [
                        ...foundFeature.properties[prop],
                        feature.properties[prop],
                      ],
                    }
                  : {};
                return {
                  ...acc,
                  ...copiedProp,
                };
              }, {})
            : {};

        // Update count and props on existing feature
        const updatedFeature = {
          ...foundFeature,
          properties: {
            ...foundFeature.properties,
            ...listifiedProps,
            count: count + 1,
          },
        };

        // Get array without selected feature
        const removeFeatureAcc = [
          ...acc.splice(0, featureIndex - 1),
          ...acc.splice(featureIndex + 1, acc.length),
        ];

        // Return new acc with updated feature
        return [...removeFeatureAcc, updatedFeature];
      }
      return acc;
    },
    [],
  );
  return createFeatureCollection(countedFeatures);
};

export { countPointsAtLocation };
