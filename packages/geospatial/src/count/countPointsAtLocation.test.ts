import { point, featureCollection } from '@turf/helpers';
import { countPointsAtLocation } from './countPointsAtLocation';

describe('countPointsAtLocation.ts', () => {
  describe('countPointsAtLocation', () => {
    it('returns FeatureCollection', () => {
      const pointFeature = point([13, 14]);
      const inputFC = featureCollection([pointFeature]);
      const countedPoints = countPointsAtLocation(inputFC);
      expect(countedPoints.type).toEqual('FeatureCollection');
    });

    it('counts number of features at each location', () => {
      const points = [
        [29, 83],
        [10, 10],
        [29, 83],
      ].map((geo) => point(geo));
      const countedPoints = countPointsAtLocation(featureCollection(points));
      expect(countedPoints.features.length == 2).toEqual(true);
      expect(
        countedPoints.features.some(
          (feature) => feature.properties['count'] === 2,
        ),
      ).toEqual(true);
    });

    it('can make lists of the properties of features in the same location', () => {
      const point1 = point([50, 50], {
        direction: 'Alexanderplatz',
        station: 'Hauptbahnhof',
        line: 'S1',
      });
      const point2 = point([50, 50], {
        direction: 'Herrmannplatz',
        station: 'Hauptbahnhof',
        line: 'U1',
      });
      const point3 = point([10, 10], {
        direction: 'Alexanderplatz',
        station: 'Hermannplatz',
        line: 'S1',
      });
      const points = [point1, point2, point3];
      const countedPoints = countPointsAtLocation(featureCollection(points), [
        'direction',
        'line',
        'doofus',
      ]);
      expect(
        countedPoints.features.some(
          // Check that one of the features has two items in the direction prop
          (feature) => feature.properties['direction'].length === 2,
        ),
      ).toEqual(true);
    });
  });
});
