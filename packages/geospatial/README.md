Currently used for transforming data, eventually will expose some useful functions.

How to run tests on different packages in different directories?

- extends
- if you put . in front of job it's hidden
- then can extend that job

How to structure/name stages jobs in this system?

- jobname:service

How to cache properly with above system?

```
- artifacts:
    paths:
    - packages/\${NAME}/node_modules
```

How do tags work with this?

- ci jobs that only run on tags
